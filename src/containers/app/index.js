import React from "react";
import NewsList from "../newslist";
import { UserConsumer, ThemeConsumer } from "../../context";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      news: {
        type: "everything",
        query: "domains=techcrunch.com&language=en",
      },
      styles: {
        display: "flex",
        align: "center",
      },
    };
  }

  render() {
    return (
      <UserConsumer.Provider value={this.state}>
        <div className="containwer-fluid">
          <div className="navbar-fixed">
            <nav>
              <div className="nav-wrapper indigo lighten-4">
                <a href="/" className="bran-logo center">
                  My Feed
                </a>
              </div>
            </nav>
          </div>
          <div className="row">
            <div className="col s12">
              <ThemeConsumer.Provider value={this.state}>
                <NewsList news={this.state.news} />
              </ThemeConsumer.Provider>
            </div>
          </div>
        </div>
      </UserConsumer.Provider>
    );
  }
}

export default App;
