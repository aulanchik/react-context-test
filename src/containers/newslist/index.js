import React from "react";
import Error from "../../components/error";
import NewsCard from "../../components/newscard";
import { ThemeConsumer } from "../../context";

class NewsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      error: false,
    };
  }

  componentDidMount() {
    const url = `https://newsapi.org/v2/${this.props.news.type}?${this.props.news.query}&apiKey=2ab44e131f2a4e9aaee192774c16e573`;

    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          news: data.articles,
        });
      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  }

  displayNews() {
    if (!this.state.error) {
      return this.state.news.map((item) => (
        <ThemeConsumer>
          {({ styles }) => (
            <div style={styles}>
              <NewsCard key={item.url} item={item} />
            </div>
          )}
        </ThemeConsumer>
      ));
    } else {
      return <Error />;
    }
  }

  render() {
    return <div className="newslist">{this.displayNews()}</div>;
  }
}

export default NewsList;
