import React from "react";
import ReactDOM from "react-dom";
import App from "./containers/app";
import "./assets/index.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
