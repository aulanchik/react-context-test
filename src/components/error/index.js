import React from "react";

const Error = () => {
  return (
    <div>
      <h1>Error occured while processing your request.</h1>
    </div>
  );
};

export default Error;
