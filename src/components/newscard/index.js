import React from "react";
import { UserConsumer } from "../../context";

const NewsCard = ({ item }) => (
  <UserConsumer>
    {({ align }) => (
      <div style={{ align }}>
        <div className="card">
          <div className="card-image">
            <img src={item.urlToImage} alt={item.title} />
          </div>
          <div className="card-content">
            <p>{item.title}</p>
            <span>by {item.author}</span>
          </div>
          <div className="card-action">
            <a href={item.url} rel="noopener noreferrer" target="_blank">
              Read More
            </a>
          </div>
        </div>
      </div>
    )}
  </UserConsumer>
);

export default NewsCard;
