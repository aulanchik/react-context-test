import React from "react";

const UserContext = React.createContext();
const ThemeContext = React.createContext();

export const UserConsumer = UserContext.Consumer;
export const ThemeConsumer = ThemeContext.Consumer;
